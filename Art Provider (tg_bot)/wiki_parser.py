import wikipedia
import requests
from bs4 import BeautifulSoup


def get_suggested1(q):
    answer = []
    text = requests.get("https://eng.wikipedia.org/wiki/" + q).content
    soup = BeautifulSoup(text, "html.parser")
    for line in soup.findAll('a'):
        if line.has_key('title'):
            if q.lower() in line.text.lower():
                if '(error: page do not exist)' not in line['title']:
                    answer.append(line['title'].replace(':', " "))
    return list(filter(lambda x: x.lower() != q.lower(), answer))


class wiki_parser:
    status = "na"

    def parse_wikipedia(self):
        search = wikipedia.search(self.query)
        if len(search) == 0:
            self.suggestion = wikipedia.suggest(self.query)
            return False
        self.search = search
        return True

    def parse(self):
        if self.ans == 1:
            if not self.parse_wikipedia():
                self.status = 0
                return False
            try:
                self.page = wikipedia.page(self.search[0])
            except wikipedia.exceptions.DisambiguationError as e:
                self.status = 2
                self.suggestion = get_suggested1(self.search[0])
                return
            self.status = 1
            return True

    def __init__(self, query):
        self.ans = 0
        if (query.lower().find('/statue') == 0) or (query.lower().find('/s') == 0):
            self.ans = 1
        elif (query.lower().find('/p') == 0) or (query.lower().find('/pic') == 0) or \
                (query.lower().find('/picture') == 0):
            self.ans = 2
        self.query = query
        self.parse()
        return
