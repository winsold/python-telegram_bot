import config
import json
import requests
import telebot
from wiki_parser import wiki_parser
from logs import get_db
from interface import menu


bot = telebot.TeleBot(config.token)
show_n_history = 3
show_n_simmilar = 3


def is_url_exists(url):
    import requests
    return requests.get(url).status_code == 200


def on_message_from_user(message_chat_id, message_text):
    logs = get_db(message_chat_id, dtype='history').write(message_text)
    try:
        wiki_info = wiki_parser(message_text)
    except:
        bot.send_message(message_chat_id, 'Internal error! Try again!')

    if wiki_info.ans == 2:
        search_string = ' '.join(wiki_info.query.split(' ')[1:])
        url_pattern = 'http://www.wikiart.org/en/search/{keyword}/1?json=2&PageSize={pageSize}'
        url = url_pattern.format(keyword=search_string, pageSize=1)
        content = requests.get(url).content

        search_results = json.loads(content, encoding='utf-8')
        if not search_results:
            bot.send_message(message_chat_id, 'Sorry! Nothing has found')
            return
        else:
            high = 3
            iters = min(len(search_results), high)
            i = 0

            while i < iters:
                painting_data = search_results[i]
                img_url = painting_data['image']

                if is_url_exists(img_url):
                    bot.send_photo(message_chat_id, img_url)
                    message_send = 'The picture: ' + '"' + painting_data['title'] + '"' + ' \nMade by: ' + \
                                   str(painting_data['artistName']) + '\nYear: ' + \
                                   str(painting_data['yearAsString']) + '.'
                    bot.send_message(message_chat_id, message_send)
                else:
                    iters = min(iters + 1, len(search_results))

                i += 1

    else:
        message_text = ' '.join(wiki_info.query.split(' ')[1:])
        if wiki_info.status == 0:
            message_send = 'Could not find anything about "' + message_text + '"' + '.\nPlease, try again!'
            bot.send_message(message_chat_id, message_send)
            return

        if wiki_info.status == 1:
            links = list(filter(lambda x: len(x) < 20, wiki_info.page.links))

            get_db(message_chat_id, dtype='page_cache').write(links)
            message_send = wiki_info.page.summary

            lis = message_text.split(' ')
            first = str(lis[0])
            res = first
            i = 1
            while i < len(lis):
                res += '-' + lis[i]
                i += 1

            bot.send_message(message_chat_id, message_send + "\n [" + wiki_info.page.url + "]")
            keyboard = menu(single_page='/main').info['reply_markup']
            bot.send_message(message_chat_id, 'Menu:', reply_markup=keyboard)
            return

        if wiki_info.status == 2:
            message_send = 'Could not find anything about "' + message_text + '"' + '. Please, try again!'
            bot.send_message(message_chat_id, message_send)
            return
        return


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    bot.send_message(message.chat.id, 'Hello! To start working please enter:\n' +
                     '</statue or /s name of artist> or\n' +
                     '</pic or /p, or /picture, and name of picture>\n' +
                     'example:\n    1) /s Salvador Dali\n    2)/p the persistance of memory')


@bot.message_handler(content_types=["text"])
def default_test(message):
    on_message_from_user(message.chat.id, message.text)
    return


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    buttons = menu(call)
    if buttons.success:
        bot.edit_message_text(**buttons.info)
        if len(buttons.search) != 0:
            on_message_from_user(call.message.chat.id, buttons.search)
        return
    return


if __name__ == '__main__':
    bot.polling(none_stop=True)
